## RainbowNewsletter


# What is RainbowNewsletter?

RainbowNewsletter is a website mainly used for announcements about Mr. Rainbow's new creations and proxies.

# Q&A

Q: Is this currently a WIP?

A: Yes.

Q: What notification service does this use?

A: PushAlert.

Q: How do I unsubscribe from RainbowNewsletter?

A: Just disable push notifications.